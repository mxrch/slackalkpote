require 'json'
require 'nokogiri'
require 'open-uri'
require 'watir'
require 'watir-scroll'
require 'awesome_print'

def translate(text)

  source = "FR"
  target = "EN"
  if source == target
    exit
  end

  puts "Lancement du naviguateur...\n\n".green

  browser = Watir::Browser.new :firefox

  final = ""

  for sentence in text.split(/(?<=[?.!])/)
    browser.goto "https://www.deepl.com/translator##{source}/#{target}/#{sentence}"
    browser.textarea(:class => ["lmt__textarea", "lmt__source_textarea", "lmt__textarea_base_style"]).wait_until_present

    #On force la séléction du langage source
    browser.div(:class => "lmt__language_select__opener").click
    browser.element(:css => "li[dl-value='#{source}']").wait_until_present
    browser.element(:css => "li[dl-value='#{source}']").click

    sleep(0.5)

    #On force la séléction du langage de destination
    browser.div(:class => "lmt__language_select__opener", :index => 1).click
    browser.element(:css => "li[dl-value='#{target}']", :index => 1).wait_until_present
    browser.element(:css => "li[dl-value='#{target}']", :index => 1).click

    sleep(2)
    translated = browser.textarea(:class => ["lmt__textarea", "lmt__target_textarea", "lmt__textarea_base_style"]).value
    if translated[translated.length - 1] == '?' && translated[translated.length - 2].downcase.between?("a", "z")
      translated.gsub!('?', ' ?')
    elsif translated[translated.length - 1] == '!' && translated[translated.length - 2].downcase.between?("a", "z")
      translated.gsub!('!', ' !')
    end
    final = final + translated
  end
  browser.close
  return final
end
