require 'sinatra'
require 'httparty'
require 'dotenv'
#require_relative 'translate'

Dotenv.load

post '/slack/alkbot' do
  request.body.rewind  # in case someone already read it
  data = JSON.parse request.body.read
  #print data
  @user = HTTParty.get("https://slack.com/api/users.profile.get?token=#{ENV['SLACK_TOKEN']}&user=#{data["event"]["user"]}&pretty=1")
  #puts "\nNOM\n"
  #print @user
  #puts "\nEn anglais : #{translate(data["event"]["text"])}"
  if data["event"]["bot_id"]
    puts "Le bot #{data["event"]["username"]} a écrit : #{data["event"]["text"]}"
  else
    puts "@#{@user["profile"]["real_name"]} a écrit : #{data["event"]["text"]}"
    HTTParty.post("https://slack.com/api/chat.postMessage?token=#{ENV['SLACK_TOKEN']}&channel=#{data["event"]["channel"]}&text=Tu viens vraiment de dire #{data["event"]["text"]} a moi ? Au grand Alkpote ?")
  end
end
